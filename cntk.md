# 21.10 <sub><sup>(2021.10.26)</sup></sub>

- Upgrade Jupyterlab to 3.2.1.

---

# V2.7.1 <sub><sup>(2021.08.23)</sup></sub>

- Update compose file.

---

# V2.7.0 <sub><sup>(2021.08.3)</sup></sub>

- Update theme.

---
