# 21.10 <sub><sup>(2021.10.28)</sup></sub>

- Upgrade CUDA to 11.4.2.
- Upgrade TensorRT to 8.03.

---

# V21.07 <sub><sup>(2021.08.23)</sup></sub>

- Upgrade CUDA to 11.4.0.
- Upgrade TensorRT to 8.0.1.6.

---
