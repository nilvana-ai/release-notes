# 1.3.0 <sub><sup>(2021.12.21)</sup></sub>

- Some enhancements and bug fixes.

---

# 1.2.1 <sub><sup>(2021.12.01)</sup></sub>

- Some enhancements and bug fixes.

---
