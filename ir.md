# V2.2.0 <sub><sup>(2023.04.11)</sup></sub>
With this release, we sync vision studio with our runtime version.

---

# V2.1.0 <sub><sup>(2023.03.17)</sup></sub>

We're pleased to announce the release of Vision Studio 2.1.0. This version adds image segmentation problem support, improves stability and performance, and syncs with our latest runtime version. With this release, users can tackle more advanced image analysis tasks and enjoy an enhanced user experience.

---

# V2.0.0 <sub><sup>(2022.12.14)</sup></sub>

With this release, we sync vision studio with our runtime version. All of the network architecture in vision studio 2.0 is now supported. In addition to network architecture support, there are numerous bug fixes for stability and performance improvements.

---

# V1.0.2 <sub><sup>(2022.09.01)</sup></sub>

- Periodic checking procedure job with numbers in queue.
- Some enhancements and bug fixes.

---

# V1.0.1 <sub><sup>(2022.08.30)</sup></sub>

- Fix memory leak problem.
- Some enhancements and bug fixes.

---

# V1.0.0 <sub><sup>(2022.07.18)</sup></sub>

- Release.

---
