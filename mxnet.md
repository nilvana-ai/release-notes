# 21.10 <sub><sup>(2021.10.25)</sup></sub>

- Upgrade Nvidia CUDA to 11.4.
- Upgrade Python3 to 3.8.
- Some enhancements and bug fixes.

---

# V1.8.1 <sub><sup>(2021.08.23)</sup></sub>

- Some enhancements and bug fixes.

---

# V1.8.0 <sub><sup>(2021.08.3)</sup></sub>

- Add gluon example.
- Update theme.

---
