# V2.2.0 <sub><sup>(2023.04.11)</sup></sub>

- Fixed the issue with the mosaic augmentation function.
- Added an annotation heatmap for object detection problems.
- Made several improvements and bug fixes to enhance performance and stability.

---

# V2.1.0 <sub><sup>(2023.03.17)</sup></sub>

- Added support for image segmentation problems, providing users with a new problem type and intelligent annotation functions.
- Included several improvements and bug fixes to enhance overall software stability and performance.

---

# V2.0.1 <sub><sup>(2022.12.21)</sup></sub>

- Some enhancements and bug fixes.

---

# V2.0.0 <sub><sup>(2022.12.14)</sup></sub>

- Introduce brand-new network architectures for the object detection problem.
- Include tiny and small network architectures for the classification problem.
- Add health-related information to version detail pages.
- Redesign the pre-trained model pages and include new models.
- A few improvements and bug fixes.

---

# V1.5.0 <sub><sup>(2022.09.02)</sup></sub>

- Support mosaic data augmentation.
- Some enhancements and bug fixes.

---

# V1.4.0 <sub><sup>(2022.07.11)</sup></sub>

- Support tensorflow export for object detection problem.
- Some enhancements and bug fixes.

---

# V1.3.0 <sub><sup>(2022.06.07)</sup></sub>

- Support ONNX export for object detection and classification problems.
- Support with choosing different pre-trained models for object detection and classification problems.
- Support split dataset export for object detection problem.
- Enhance annotation performance for large images.
- Some enhancements and bug fixes.

---

# V1.2.0 <sub><sup>(2022.01.21)</sup></sub>

- Responsive Web Design (RWD).
- Some enhancements and bug fixes for annotation, version and CAM.
- Some new descriptions to improve the understanding of some features.

---

# V1.1.0 <sub><sup>(2022.01.04)</sup></sub>

- Support class activation maps (CAM) for classification problem.
- Support IoU threshold for object detection endpoint.
- Add hyperparameter information to model detail.
- Enhance dataset import function.
- Enhance version generation performance.
- Some enhancements and bug fixes for annotation.
- Some new descriptions to improve the understanding of some features.

---

# V1.0.0 <sub><sup>(2021.12.22)</sup></sub>

- Support image classification problem.
- Redesign data augmentation feature.
- Redesign version detail pages.
- Redesign model detail pages.
- Redesign model evaluation pages.
- Support multiple images validation.
- Support hyperparameter tuning for model training.
- Support CSV export for training diagrams.
- Allow password reset for administrator account.
- Approve semi-automatic annotation boxes with one click.
- Some enhancements and bug fixes for model training and annotation.
- Some new descriptions to improve the understanding of some features.

---

# V0.13.0 <sub><sup>(2021.10.06)</sup></sub>

- Update database schema to version 2.
- Some enhancements and bug fixes for model training.
- Support CVAT annotation format for object detection problem.
- Redesign augmentation component.
- Some new texts to improve the understanding of some features.

---

# V0.12.1 <sub><sup>(2021.08.11)</sup></sub>

- Some enhancements and bug fixes.

---
