# 20.10 <sub><sup>(2021.10.26)</sup></sub>

- Upgrade Jupypterlab to 3.2.1.

# V1.9.1b <sub><sup>(2021.10.22)</sup></sub>

- Add `TensorBoard` support.
- Set default shell to `bash`.
- Add NLP related package.
- Create Python3 & pip3 Alias.
- Upgrade `pytoch-lightning` to 1.4.9.

---

# V1.9.1a <sub><sup>(2021.09.27)</sup></sub>

- Add `pytoch-lightning` support.
- Upgrade `pillow` to 8.2.0.
- Upgrade `PyTorch` to 1.9.1.
- Upgrade `FastAI` to 2.5.2

---
