# 21.10 <sub><sup>(2021.10.25)</sup></sub>

- Upgrade Jupyterlab to 3.x.

---

# 21.08 <sub><sup>(2021.08.23)</sup></sub>

- Some enhancements and bug fixes.

---
