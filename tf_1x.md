# 21.10 <sub><sup>(2021.10.25)</sup></sub>

- Upgrade Nvidia CUDA to 11.2.
- Set default shell to `bash`.
- Upgrade Jupyterlab to 2.2.9.
- Remove `tensorboard`.

---

# V4.0.0 <sub><sup>(2021.08.23)</sup></sub>

- Theme upddate.

---

# V3.0.2 <sub><sup>(2021.08.10)</sup></sub>

- Fix autocomplete.

---
