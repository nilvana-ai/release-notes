# 21.10 <sub><sup>(2021.10.25)</sup></sub>

- Upgrade Nvidia CUDA to 11.4.
- Upgrade Python3 to 3.8.
- Upgrade tensorflow to 2.7rc1.
- Upgrade Jupyterlab to 3.2.1.
- Set default shell to `bash`.

---

# V4.0.1 <sub><sup>(2021.08.31)</sup></sub>

- Upgrade tensorflow to 2.6.

---

# V4.0.0 <sub><sup>(2021.08.23)</sup></sub>

- Fix autocomplete.
- Add decision forest package.

---
